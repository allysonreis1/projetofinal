
package model;

import java.util.ArrayList;

public class Alimentacao {
    private ArrayList<String> listaRefeicoes;
    private Boolean almoco;
    private Boolean jantar;
    private Boolean cafe;

    public ArrayList<String> getListaRefeicoes() {
        return listaRefeicoes;
    }

    public void setListaRefeicoes(ArrayList<String> listaRefeicoes) {
        this.listaRefeicoes = listaRefeicoes;
    }

    public Boolean getAlmoco() {
        return almoco;
    }

    public void setAlmoco(Boolean almoco) {
        this.almoco = almoco;
    }

    public Boolean getJantar() {
        return jantar;
    }

    public void setJantar(Boolean jantar) {
        this.jantar = jantar;
    }

    public Boolean getCafe() {
        return cafe;
    }

    public void setCafe(Boolean cafe) {
        this.cafe = cafe;
    }
}
