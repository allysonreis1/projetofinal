
package model;

public class Quarto {
    private int numeroHospedes;
    private Boolean quartoSuite;

    public int getNumeroHospedes() {
        return numeroHospedes;
    }

    public void setNumeroHospedes(int numeroHospedes) {
        this.numeroHospedes = numeroHospedes;
    }

    public Boolean isQuartoSuite() {
        return quartoSuite;
    }

    public void setQuartoSuite(Boolean quartoSuite) {
        this.quartoSuite = quartoSuite;
    }
    
    
}
