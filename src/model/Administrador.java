
package model;

import java.util.ArrayList;

public class Administrador extends Cliente{
    private String numeroCadastro; // número do cadastro na empresa

    public Administrador (String nome, String email, String senha, ArrayList<String> listaTelefones,
            String numeroCadastro) {
        super(nome, email, senha, listaTelefones);
        this.numeroCadastro = numeroCadastro;
    }
    
    
    public String getNumeroCadastro() {
        return numeroCadastro;
    }

    public void setNumeroCadastro(String numeroCadastro) {
        this.numeroCadastro = numeroCadastro;
    }
    
    
}
