
package model;

import java.util.ArrayList;


public class Resort extends Hospedagem{
    private ArrayList<String> listaEsportes; //Poderia ser um objeto Esporte
    private ArrayList<String> listaEntretenimento;

    public ArrayList<String> getListaEsportes() {
        return listaEsportes;
    }

    public void setListaEsportes(ArrayList<String> listaEsportes) {
        this.listaEsportes = listaEsportes;
    }

    public ArrayList<String> getListaEntretenimento() {
        return listaEntretenimento;
    }

    public void setListaEntretenimento(ArrayList<String> listaEntretenimento) {
        this.listaEntretenimento = listaEntretenimento;
    }
    
    
}
