
package model;

import java.util.ArrayList;

public class Cliente {
    private String nome;
    private String cpf;
    private String rg;
    private Endereco endereco;
    private ArrayList<String> listaTelefones;
    private String dataNascimento;
    private String senha;
    private String email;
    private ArrayList<Hospedagem> listaHospedagem;
    
    public Cliente (String nome, String email, String senha, ArrayList<String> listaTelefones) {
        this.email = email;
        this.nome = nome;
        this.senha = senha;
        this.listaTelefones = listaTelefones;
    }

    public ArrayList<Hospedagem> getListaHospedagem() {
        return listaHospedagem;
    }

    public void setListaHospedagem(ArrayList<Hospedagem> listaHospedagem) {
        this.listaHospedagem = listaHospedagem;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<String> getListaTelefones() {
        return listaTelefones;
    }

    public void setListaTelefones(ArrayList<String> listaTelefones) {
        this.listaTelefones = listaTelefones;
    }
    
    
}
