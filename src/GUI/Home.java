/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import controller.ControleHospedagem;
import controller.ControleHotel;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Administrador;
import model.Cliente;
import model.Hospedagem;
import model.Hotel;

/**
 *
 * @author reis
 */
public class Home extends javax.swing.JDialog {

    private Cliente umCliente;
    private Administrador umAdministrador;
    private DefaultListModel telefonesListModel;
    private ArrayList<String> umaListaDeTelefones;
    private boolean novoHotel;
    private Hotel umHotel;
    private ControleHotel umControleHotel;
    private ArrayList<Hotel> umaListaHotel;
    private ControleHospedagem umControleHospedagem;
    
    public Home(java.awt.Frame parent, boolean modal, Cliente umCliente, Administrador umAdministrador,
            ControleHotel umControleHotel, ControleHospedagem umControleHospedagem) {
        super(parent, modal);
        initComponents();
        telefonesListModel = new DefaultListModel();
        jListListaTelefones.setModel(telefonesListModel);
        umaListaDeTelefones = new ArrayList<String>();
        umaListaHotel = new ArrayList<Hotel>();
        this.umControleHotel = new ControleHotel();
        this.umControleHospedagem = new ControleHospedagem();
        this.umCliente = umCliente;
        this.umAdministrador = umAdministrador;
        this.umControleHotel = umControleHotel;
        this.umControleHospedagem = umControleHospedagem;
        preencherDados();
        habilitarAdministrador();
        habilitarCliente();
        carregarListaHoteis();
        //carregarMinhaViagem();
    }

    public ControleHotel getUmControleHotel() {
        return umControleHotel;
    }

    public ControleHospedagem getUmControleHospedagem() {
        return umControleHospedagem;
    }
    
    
    public void habilitarAdministrador () {
        if (this.umAdministrador != null) {
            jButtonExcluirUsuario.setEnabled(true);
            jButtonPesquisarCliente.setEnabled(true);
            jButtonSalvarTudo.setEnabled(true);
            
            jTextFieldDuracaoAerea.setEnabled(true);
            jTextFieldDuracaoOnibus.setEnabled(true);
            jTextFieldEmpresaAerea.setEnabled(true);
            jTextFieldEmpresaOnibus.setEnabled(true);
            jTextFieldValorAerea.setEnabled(true);
            jTextFieldValorOnibus.setEnabled(true);
            jButtonNovoHotel.setEnabled(true);
        }
    }
    
    public void habilitarCliente () {
        if (this.umCliente != null) {
            jButtonHospedarHotel.setEnabled(true);
        }
    }
    
    public void salvarTudo () {
        if (this.novoHotel == true) {
            umHotel = new Hotel();
            umHotel.setBairro(jTextFieldBairroHotel.getText());
            umHotel.setCep(jTextFieldCepHotel.getText());
            
            int estrelas = Integer.parseInt(jTextFieldQuantidadeEstrelasHotel.getText());
            umHotel.setEstrelas(estrelas);
            umHotel.setNome(jTextFieldNomeHotel.getText());
            
            int quartos = Integer.parseInt(jTextFieldQuartosDispHotel.getText());
            umHotel.setQuartosDisponiveis(quartos);
            umHotel.setRua(jTextFieldRuaHotel.getText());
            umHotel.setUf(jComboBoxUfHotel.getSelectedItem().toString());
            umHotel.setCidade(jTextFieldCidadeHotel.getText());
            umHotel.setNumero(jTextFieldNumeroHotel.getText());
            
            umControleHotel.adicionarHotel(umHotel);
        } else {
            umHotel.setBairro(jTextFieldBairroHotel.getText());
            umHotel.setCep(jTextFieldCepHotel.getText());
            
            int estrelas = Integer.parseInt(jTextFieldQuantidadeEstrelasHotel.getText());
            umHotel.setEstrelas(estrelas);
            umHotel.setNome(jTextFieldNomeHotel.getText());
            
            int quartos = Integer.parseInt(jTextFieldQuartosDispHotel.getText());
            umHotel.setQuartosDisponiveis(quartos);
            umHotel.setRua(jTextFieldRuaHotel.getText());
            umHotel.setUf(jComboBoxUfHotel.getSelectedItem().toString());
            
            umHotel.setCidade(jTextFieldCidadeHotel.getText());
            umHotel.setNumero(jTextFieldNumeroHotel.getText());
        }
    }
    
    public void preencherHotel () {
        jTextFieldBairroHotel.setText(umHotel.getBairro());
        jTextFieldCepHotel.setText(umHotel.getCep());
        jTextFieldCidadeHotel.setText(umHotel.getCidade());
        jTextFieldNomeHotel.setText(umHotel.getNome());
        jTextFieldNumeroHotel.setText(umHotel.getNumero());
        
        String quartos = Integer.toString(umHotel.getQuartosDisponiveis());
        jTextFieldQuartosDispHotel.setText(quartos);
        jTextFieldRuaHotel.setText(umHotel.getRua());
        
        String estrelas = Integer.toString(umHotel.getEstrelas());
        jTextFieldQuantidadeEstrelasHotel.setText(estrelas);
    }
    
    private void carregarListaHoteis () {
        ArrayList<Hotel> listaHoteis = umControleHotel.getListaHoteis();
        DefaultTableModel model = (DefaultTableModel) jTableListaHotel.getModel();
        model.setRowCount(0);
        for (Hotel hotel : listaHoteis) {
            String estrelas = Integer.toString(hotel.getEstrelas());
            model.addRow(new String[]{hotel.getNome(), estrelas});
        }
    }
    
    private void carregarMinhaViagem () {
        ArrayList<Hospedagem> listaHoteis = umCliente.getListaHospedagem();
        DefaultTableModel model = (DefaultTableModel) jTableListaMinhaViagem.getModel();
        model.setRowCount(0);
        for (Hospedagem hospedagem : listaHoteis) {
            model.addRow(new String[]{hospedagem.getNome(), hospedagem.getCidade(), null});
        }
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void preencherDados () {
        ArrayList<String> telefones;
        if (this.umCliente != null && this.umAdministrador == null) {
            jTextFieldNomeMViagem.setText(umCliente.getNome());
            jTextFieldCpfMViagem.setText(umCliente.getCpf());
            jTextFieldRgMViagem.setText(umCliente.getRg());
            jTextFieldEmailMViagem.setText(umCliente.getEmail());
            
            telefonesListModel.clear();
            umaListaDeTelefones = umCliente.getListaTelefones();
            telefones = umCliente.getListaTelefones();
            
            for (String umTelefone : telefones) {
                telefonesListModel.addElement(umTelefone);
            }
            
        } else if (this.umAdministrador != null && this.umCliente == null) {
            jTextFieldNomeMViagem.setText(umAdministrador.getNome());
            jTextFieldCpfMViagem.setText(umAdministrador.getCpf());
            jTextFieldRgMViagem.setText(umAdministrador.getRg());
            jTextFieldEmailMViagem.setText(umAdministrador.getEmail());
            jTextFieldCodigoCadastroMViagens.setText(umAdministrador.getNumeroCadastro());
            
            telefonesListModel.clear();
            umaListaDeTelefones = umAdministrador.getListaTelefones();
            telefones = umAdministrador.getListaTelefones();
            
            for (String umTelefone : telefones) {
                telefonesListModel.addElement(umTelefone);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListaHotel = new javax.swing.JTable();
        jTextFieldCidadeHotel = new javax.swing.JTextField();
        jTextFieldQuartosDispHotel = new javax.swing.JTextField();
        jTextFieldCepHotel = new javax.swing.JTextField();
        jTextFieldBairroHotel = new javax.swing.JTextField();
        jTextFieldNumeroHotel = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldNomeHotel = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldQuantidadeEstrelasHotel = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jComboBoxUfHotel = new javax.swing.JComboBox();
        jTextFieldRuaHotel = new javax.swing.JTextField();
        jButtonHospedarHotel = new javax.swing.JButton();
        jButtonNovoHotel = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jTextFieldEmpresaOnibus = new javax.swing.JTextField();
        jTextFieldEmpresaAerea = new javax.swing.JTextField();
        jTextFieldValorAerea = new javax.swing.JTextField();
        jTextFieldValorOnibus = new javax.swing.JTextField();
        jButtonComprarPassagem = new javax.swing.JButton();
        jLabelDuracao = new javax.swing.JLabel();
        jLabelDuracao2 = new javax.swing.JLabel();
        jTextFieldDuracaoAerea = new javax.swing.JTextField();
        jTextFieldDuracaoOnibus = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jListResorts = new javax.swing.JList();
        jLabelNomeResort = new javax.swing.JLabel();
        jTextFieldNomeResort = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        jButtonAdiconarEsporte = new javax.swing.JButton();
        jButtonRemoverEsporte = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        jButtonAdicionarEntretenimento = new javax.swing.JButton();
        jButtonRemoverEntretenimento = new javax.swing.JButton();
        jLabelEsportes = new javax.swing.JLabel();
        jLabelEntretenimento = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jTextFieldCepResort = new javax.swing.JTextField();
        jTextFieldBairroResort = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jTextFieldRuaResort = new javax.swing.JTextField();
        jTextFieldQuartoDisponivel = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableListaMinhaViagem = new javax.swing.JTable();
        jButtonCancelarMinhaViagem = new javax.swing.JButton();
        jButtonAlterarDados = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldNomeMViagem = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldCpfMViagem = new javax.swing.JTextField();
        jTextFieldRgMViagem = new javax.swing.JTextField();
        jTextFieldEmailMViagem = new javax.swing.JTextField();
        jButtonCancelarMViagem = new javax.swing.JButton();
        jButtonSalvarMViagem = new javax.swing.JButton();
        jLabelTelefoneMViagem = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListListaTelefones = new javax.swing.JList();
        jButtonAdcionarTelefoneMViagem = new javax.swing.JButton();
        jButtonRemoverTelefoneMViagem = new javax.swing.JButton();
        jButtonAlterarEndereco = new javax.swing.JButton();
        jLabelDataEmbarque = new javax.swing.JLabel();
        jFormattedTextFieldDataEmbarqueMViagem = new javax.swing.JFormattedTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextFieldCodigoCadastroMViagens = new javax.swing.JTextField();
        jButtonSair = new javax.swing.JButton();
        jButtonExcluirUsuario = new javax.swing.JButton();
        jButtonPesquisarCliente = new javax.swing.JButton();
        jButtonPesquisarCidade = new javax.swing.JButton();
        jButtonSalvarTudo = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTableListaHotel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Estrelas"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableListaHotel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaHotelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableListaHotel);

        jTextFieldCidadeHotel.setEnabled(false);

        jTextFieldQuartosDispHotel.setEnabled(false);

        jTextFieldCepHotel.setEnabled(false);

        jTextFieldBairroHotel.setEnabled(false);

        jTextFieldNumeroHotel.setEnabled(false);

        jLabel1.setText("Cidade");

        jLabel2.setText("UF");

        jLabel3.setText("Quartos Disponíveis");

        jLabel4.setText("Nome do Hotel");

        jTextFieldNomeHotel.setEnabled(false);

        jLabel5.setText("Estrelas");

        jTextFieldQuantidadeEstrelasHotel.setEnabled(false);

        jLabel6.setText("Cep");

        jLabel7.setText("Bairro");

        jLabel8.setText("Número");

        jLabel9.setText("Rua");

        jComboBoxUfHotel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC(\"Acre\")", "AL(\"Alagoas\")", "AM(\"Amazonas\")", "AP(\"Amapá\")", "BA(\"Bahia\")", "CE(\"Ceará\")", "DF(\"Distrito Federal\")", "ES(\"Espirito Santo\")", "GO(\"Goias\")", "MA(\"Maranhão\")", "MG(\"Minas Gerais\")", "MS(\"Mato Grosso Sul\")", "MT(\"Mato Grosso\")", "PA(\"Pará\")", "PB(\"Paraiba\")", "PE(\"Pernanbuco\")", "PI(\"Piaui\")", "PR(\"Parana\")", "RJ(\"Rio de Janeiro\")", "RN(\"Rio Grande do Norte\")", "RO(\"Rondônia\")", "RR(\"Roraima\")", "RS(\"Rio Grande do Sul\")", "SC(\"Santa Catarina\")", "SE(\"Sergipe\")", "SP(\"São Paulo\")", "TO(\"Tocantins\")" }));

        jTextFieldRuaHotel.setEnabled(false);

        jButtonHospedarHotel.setText("Hospedar");
        jButtonHospedarHotel.setEnabled(false);
        jButtonHospedarHotel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHospedarHotelActionPerformed(evt);
            }
        });

        jButtonNovoHotel.setText("Novo Hotel");
        jButtonNovoHotel.setEnabled(false);
        jButtonNovoHotel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoHotelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel8)
                                .addComponent(jLabel7)
                                .addComponent(jLabel6))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldNumeroHotel, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextFieldCepHotel, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                                .addComponent(jTextFieldBairroHotel))
                            .addGap(18, 18, 18)
                            .addComponent(jLabel9)
                            .addGap(18, 18, 18)
                            .addComponent(jTextFieldRuaHotel, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextFieldQuartosDispHotel, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel2))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldCidadeHotel, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                        .addComponent(jComboBoxUfHotel, 0, 1, Short.MAX_VALUE)))
                                .addComponent(jButtonNovoHotel, javax.swing.GroupLayout.Alignment.LEADING)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldNomeHotel, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldQuantidadeEstrelasHotel)))
                    .addComponent(jButtonHospedarHotel))
                .addContainerGap(69, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldCepHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel9)
                            .addComponent(jTextFieldRuaHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldBairroHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonNovoHotel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jTextFieldCidadeHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jComboBoxUfHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldQuartosDispHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextFieldNumeroHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextFieldNomeHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jTextFieldQuantidadeEstrelasHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonHospedarHotel)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Hotel", jPanel1);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Empresa", "Tipo", "Valor", "Duração"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(jTable3);

        jLabel14.setText("Empresa Aérea");

        jLabel15.setText("Empresa de Ônibus");

        jLabel16.setText("Valor");
        jLabel16.setToolTipText("");

        jLabel17.setText("Valor");

        jTextFieldEmpresaOnibus.setEnabled(false);

        jTextFieldEmpresaAerea.setEnabled(false);

        jTextFieldValorAerea.setEnabled(false);

        jTextFieldValorOnibus.setEnabled(false);

        jButtonComprarPassagem.setText("Comprar");

        jLabelDuracao.setText("Duração");

        jLabelDuracao2.setText("Duração");

        jTextFieldDuracaoAerea.setEnabled(false);

        jTextFieldDuracaoOnibus.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonComprarPassagem)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel14))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextFieldEmpresaAerea, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                                    .addComponent(jTextFieldEmpresaOnibus))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel17))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextFieldValorAerea, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                                    .addComponent(jTextFieldValorOnibus))
                                .addGap(34, 34, 34)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabelDuracao)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldDuracaoAerea))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabelDuracao2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldDuracaoOnibus, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel16)
                    .addComponent(jTextFieldEmpresaAerea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldValorAerea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDuracao)
                    .addComponent(jTextFieldDuracaoAerea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel17)
                    .addComponent(jTextFieldEmpresaOnibus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldValorOnibus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDuracao2)
                    .addComponent(jTextFieldDuracaoOnibus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 106, Short.MAX_VALUE)
                .addComponent(jButtonComprarPassagem)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Passagem", jPanel2);

        jScrollPane7.setViewportView(jListResorts);

        jLabelNomeResort.setText("Resort");

        jTextFieldNomeResort.setEnabled(false);

        jButtonAdiconarEsporte.setText("Adicionar");
        jButtonAdiconarEsporte.setEnabled(false);

        jButtonRemoverEsporte.setText("Remover");
        jButtonRemoverEsporte.setEnabled(false);

        jButtonAdicionarEntretenimento.setText("Adicionar");
        jButtonAdicionarEntretenimento.setEnabled(false);

        jButtonRemoverEntretenimento.setText("Remover");
        jButtonRemoverEntretenimento.setEnabled(false);

        jLabelEsportes.setText("Esportes");

        jLabelEntretenimento.setText("Entretenimentos");

        jLabel19.setText("cep");

        jLabel20.setText("Bairro");

        jLabel21.setText("UF");

        jLabel22.setText("Rua");

        jLabel23.setText("Quartos Disponíveis");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC(\"Acre\")", "AL(\"Alagoas\")", "AM(\"Amazonas\")", "AP(\"Amapá\")", "BA(\"Bahia\")", "CE(\"Ceará\")", "DF(\"Distrito Federal\")", "ES(\"Espirito Santo\")", "GO(\"Goias\")", "MA(\"Maranhão\")", "MG(\"Minas Gerais\")", "MS(\"Mato Grosso Sul\")", "MT(\"Mato Grosso\")", "PA(\"Pará\")", "PB(\"Paraiba\")", "PE(\"Pernanbuco\")", "PI(\"Piaui\")", "PR(\"Parana\")", "RJ(\"Rio de Janeiro\")", "RN(\"Rio Grande do Norte\")", "RO(\"Rondônia\")", "RR(\"Roraima\")", "RS(\"Rio Grande do Sul\")", "SC(\"Santa Catarina\")", "SE(\"Sergipe\")", "SP(\"São Paulo\")", "TO(\"Tocantins\")" }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                            .addComponent(jScrollPane8))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelNomeResort)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel20))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextFieldNomeResort, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                                    .addComponent(jTextFieldCepResort)
                                    .addComponent(jTextFieldBairroResort))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel21)
                                            .addComponent(jLabel22))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldRuaResort, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jComboBox2, 0, 1, Short.MAX_VALUE)))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(jLabel23)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldQuartoDisponivel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButtonAdicionarEntretenimento)
                                    .addComponent(jButtonRemoverEntretenimento))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelEsportes)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButtonAdiconarEsporte)
                                            .addComponent(jButtonRemoverEsporte))))))
                        .addGap(80, 80, 80))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabelEntretenimento)
                        .addGap(136, 136, 136))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNomeResort)
                            .addComponent(jTextFieldNomeResort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(jTextFieldCepResort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel22)
                            .addComponent(jTextFieldRuaResort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(jTextFieldBairroResort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel23)
                            .addComponent(jTextFieldQuartoDisponivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 143, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEntretenimento)
                    .addComponent(jLabelEsportes))
                .addGap(1, 1, 1)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jButtonAdicionarEntretenimento)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonRemoverEntretenimento))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jButtonAdiconarEsporte)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonRemoverEsporte)))
                .addGap(36, 36, 36))
        );

        jTabbedPane1.addTab("Resort", jPanel3);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(jTable5);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 681, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(213, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Pacote", jPanel4);

        jTableListaMinhaViagem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Local", "Passagem"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableListaMinhaViagem);

        jButtonCancelarMinhaViagem.setText("Cancelar Viagem");

        jButtonAlterarDados.setText("Alterar Dados Pessoais");

        jLabel10.setText("Nome");

        jTextFieldNomeMViagem.setEnabled(false);

        jLabel11.setText("Cpf");

        jLabel12.setText("Rg");

        jLabel13.setText("Email");

        jTextFieldCpfMViagem.setEnabled(false);

        jTextFieldRgMViagem.setEnabled(false);

        jTextFieldEmailMViagem.setEnabled(false);

        jButtonCancelarMViagem.setText("Cancelar");

        jButtonSalvarMViagem.setText("Salvar");

        jLabelTelefoneMViagem.setText("Telefone");

        jListListaTelefones.setEnabled(false);
        jScrollPane3.setViewportView(jListListaTelefones);

        jButtonAdcionarTelefoneMViagem.setText("+");
        jButtonAdcionarTelefoneMViagem.setEnabled(false);

        jButtonRemoverTelefoneMViagem.setText("-");
        jButtonRemoverTelefoneMViagem.setEnabled(false);

        jButtonAlterarEndereco.setText("Alterar Meu Endereço");

        jLabelDataEmbarque.setText("Data de Embarque");

        try {
            jFormattedTextFieldDataEmbarqueMViagem.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextFieldDataEmbarqueMViagem.setEnabled(false);

        jLabel18.setText("Código de cadastro");

        jTextFieldCodigoCadastroMViagens.setEnabled(false);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jButtonCancelarMinhaViagem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAlterarDados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAlterarEndereco)
                        .addGap(85, 85, 85)
                        .addComponent(jButtonSalvarMViagem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonCancelarMViagem))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel10)
                                .addComponent(jLabel11)
                                .addComponent(jLabel12)
                                .addComponent(jLabel13))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldEmailMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextFieldRgMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextFieldCpfMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextFieldNomeMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabelTelefoneMViagem)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelDataEmbarque)
                                        .addComponent(jLabel18))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jFormattedTextFieldDataEmbarqueMViagem, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                                        .addComponent(jTextFieldCodigoCadastroMViagens))))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButtonRemoverTelefoneMViagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonAdcionarTelefoneMViagem, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)))
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 678, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextFieldNomeMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDataEmbarque)
                    .addComponent(jFormattedTextFieldDataEmbarqueMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextFieldCpfMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jTextFieldCodigoCadastroMViagens, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jTextFieldRgMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jTextFieldEmailMViagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelTelefoneMViagem)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jButtonAdcionarTelefoneMViagem)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonRemoverTelefoneMViagem)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancelarMinhaViagem)
                    .addComponent(jButtonAlterarDados)
                    .addComponent(jButtonCancelarMViagem)
                    .addComponent(jButtonSalvarMViagem)
                    .addComponent(jButtonAlterarEndereco))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Minhas Viagens", jPanel5);

        jButtonSair.setText("Sair");
        jButtonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSairActionPerformed(evt);
            }
        });

        jButtonExcluirUsuario.setText("Excluir Usuário");
        jButtonExcluirUsuario.setEnabled(false);

        jButtonPesquisarCliente.setText("Pesquisar Cliente");
        jButtonPesquisarCliente.setEnabled(false);

        jButtonPesquisarCidade.setText("Pesquisar Cidade");

        jButtonSalvarTudo.setText("Salvar");
        jButtonSalvarTudo.setEnabled(false);
        jButtonSalvarTudo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarTudoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 728, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButtonPesquisarCidade)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonExcluirUsuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonPesquisarCliente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonSalvarTudo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSair, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 12, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSair)
                    .addComponent(jButtonExcluirUsuario)
                    .addComponent(jButtonPesquisarCliente)
                    .addComponent(jButtonPesquisarCidade)
                    .addComponent(jButtonSalvarTudo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 435, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSalvarTudoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarTudoActionPerformed
        salvarTudo();
        carregarListaHoteis();
        jTextFieldBairroHotel.setText(null);
        jTextFieldCepHotel.setText(null);
        jTextFieldCidadeHotel.setText(null);
        jTextFieldNomeHotel.setText(null);
        jTextFieldNumeroHotel.setText(null);
        jTextFieldQuartosDispHotel.setText(null);
        jTextFieldRuaHotel.setText(null);
        jTextFieldQuantidadeEstrelasHotel.setText(null);
        
        jTextFieldBairroHotel.setEnabled(false);
        jTextFieldCepHotel.setEnabled(false);
        jTextFieldCidadeHotel.setEnabled(false);
        jTextFieldNomeHotel.setEnabled(false);
        jTextFieldNumeroHotel.setEnabled(false);
        jTextFieldQuartosDispHotel.setEnabled(false);
        jTextFieldQuantidadeEstrelasHotel.setEnabled(false);
        jTextFieldRuaHotel.setEnabled(false);
    }//GEN-LAST:event_jButtonSalvarTudoActionPerformed

    private void jButtonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSairActionPerformed
        this.dispose();
        this.umAdministrador = null;
        this.umCliente = null;
    }//GEN-LAST:event_jButtonSairActionPerformed

    private void jButtonNovoHotelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoHotelActionPerformed
        this.novoHotel = true;
        jTextFieldBairroHotel.setText(null);
        jTextFieldCepHotel.setText(null);
        jTextFieldCidadeHotel.setText(null);
        jTextFieldNomeHotel.setText(null);
        jTextFieldNumeroHotel.setText(null);
        jTextFieldQuartosDispHotel.setText(null);
        jTextFieldRuaHotel.setText(null);
        jTextFieldQuantidadeEstrelasHotel.setText(null);
        
        jTextFieldBairroHotel.setEnabled(true);
        jTextFieldCepHotel.setEnabled(true);
        jTextFieldCidadeHotel.setEnabled(true);
        jTextFieldNomeHotel.setEnabled(true);
        jTextFieldNumeroHotel.setEnabled(true);
        jTextFieldQuartosDispHotel.setEnabled(true);
        jTextFieldQuantidadeEstrelasHotel.setEnabled(true);
        jTextFieldRuaHotel.setEnabled(true);
    }//GEN-LAST:event_jButtonNovoHotelActionPerformed

    private void jTableListaHotelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaHotelMouseClicked
        DefaultTableModel model = (DefaultTableModel) jTableListaHotel.getModel();
        String umNome = (String) model.getValueAt(jTableListaHotel.getSelectedRow(), 0);
        umHotel = umControleHotel.pesquisarHotel(umNome);
        preencherHotel();
    }//GEN-LAST:event_jTableListaHotelMouseClicked

    private void jButtonHospedarHotelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHospedarHotelActionPerformed
        
        umControleHospedagem.adicionarHospedagem(umHotel);
        ArrayList<Hospedagem> hospedagens = umControleHospedagem.getListaHospedagem();
        umCliente.setListaHospedagem(hospedagens);
        carregarMinhaViagem();
    }//GEN-LAST:event_jButtonHospedarHotelActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Home dialog = new Home(new javax.swing.JFrame(), true, null, null, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdcionarTelefoneMViagem;
    private javax.swing.JButton jButtonAdicionarEntretenimento;
    private javax.swing.JButton jButtonAdiconarEsporte;
    private javax.swing.JButton jButtonAlterarDados;
    private javax.swing.JButton jButtonAlterarEndereco;
    private javax.swing.JButton jButtonCancelarMViagem;
    private javax.swing.JButton jButtonCancelarMinhaViagem;
    private javax.swing.JButton jButtonComprarPassagem;
    private javax.swing.JButton jButtonExcluirUsuario;
    private javax.swing.JButton jButtonHospedarHotel;
    private javax.swing.JButton jButtonNovoHotel;
    private javax.swing.JButton jButtonPesquisarCidade;
    private javax.swing.JButton jButtonPesquisarCliente;
    private javax.swing.JButton jButtonRemoverEntretenimento;
    private javax.swing.JButton jButtonRemoverEsporte;
    private javax.swing.JButton jButtonRemoverTelefoneMViagem;
    private javax.swing.JButton jButtonSair;
    private javax.swing.JButton jButtonSalvarMViagem;
    private javax.swing.JButton jButtonSalvarTudo;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBoxUfHotel;
    private javax.swing.JFormattedTextField jFormattedTextFieldDataEmbarqueMViagem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelDataEmbarque;
    private javax.swing.JLabel jLabelDuracao;
    private javax.swing.JLabel jLabelDuracao2;
    private javax.swing.JLabel jLabelEntretenimento;
    private javax.swing.JLabel jLabelEsportes;
    private javax.swing.JLabel jLabelNomeResort;
    private javax.swing.JLabel jLabelTelefoneMViagem;
    private javax.swing.JList jListListaTelefones;
    private javax.swing.JList jListResorts;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTableListaHotel;
    private javax.swing.JTable jTableListaMinhaViagem;
    private javax.swing.JTextField jTextFieldBairroHotel;
    private javax.swing.JTextField jTextFieldBairroResort;
    private javax.swing.JTextField jTextFieldCepHotel;
    private javax.swing.JTextField jTextFieldCepResort;
    private javax.swing.JTextField jTextFieldCidadeHotel;
    private javax.swing.JTextField jTextFieldCodigoCadastroMViagens;
    private javax.swing.JTextField jTextFieldCpfMViagem;
    private javax.swing.JTextField jTextFieldDuracaoAerea;
    private javax.swing.JTextField jTextFieldDuracaoOnibus;
    private javax.swing.JTextField jTextFieldEmailMViagem;
    private javax.swing.JTextField jTextFieldEmpresaAerea;
    private javax.swing.JTextField jTextFieldEmpresaOnibus;
    private javax.swing.JTextField jTextFieldNomeHotel;
    private javax.swing.JTextField jTextFieldNomeMViagem;
    private javax.swing.JTextField jTextFieldNomeResort;
    private javax.swing.JTextField jTextFieldNumeroHotel;
    private javax.swing.JTextField jTextFieldQuantidadeEstrelasHotel;
    private javax.swing.JTextField jTextFieldQuartoDisponivel;
    private javax.swing.JTextField jTextFieldQuartosDispHotel;
    private javax.swing.JTextField jTextFieldRgMViagem;
    private javax.swing.JTextField jTextFieldRuaHotel;
    private javax.swing.JTextField jTextFieldRuaResort;
    private javax.swing.JTextField jTextFieldValorAerea;
    private javax.swing.JTextField jTextFieldValorOnibus;
    // End of variables declaration//GEN-END:variables
}
