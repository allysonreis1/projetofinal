
package controller;

import java.util.ArrayList;
import model.Hospedagem;

public class ControleHospedagem {
    private ArrayList<Hospedagem> listaHospedagem;
    
    public ControleHospedagem () {
        listaHospedagem = new ArrayList<Hospedagem>();
    }
    
    public ArrayList<Hospedagem> getListaHospedagem() {
        return listaHospedagem;
    }
    
    public void adicionarHospedagem (Hospedagem umaHospedagem) {
        this.listaHospedagem.add(umaHospedagem);
    }
    
    public void removerHospedagem (Hospedagem umaHospedagem){
        this.listaHospedagem.remove(umaHospedagem);
    }
}
