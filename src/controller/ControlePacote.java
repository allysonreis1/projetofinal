
package controller;

import java.util.ArrayList;
import model.Pacote;

public class ControlePacote {
    private ArrayList<Pacote> listaPacotes;
    
    public void adicionarPacote (Pacote pacoteViagem) {
        this.listaPacotes.add(pacoteViagem);
    }
    
    public void removerPacote (Pacote pacoteViagem) {
        this.listaPacotes.remove(pacoteViagem);
    }
    
    public Pacote pesquisarPacote (String umNome, String umCep) {
        for (Pacote pacotePesquisado : listaPacotes) {
            if (pacotePesquisado.getNome().equalsIgnoreCase(umNome) && 
                    pacotePesquisado.getCep().equalsIgnoreCase(umCep)) {
                
                return pacotePesquisado;
            }
        }
        return null;
    }
}
