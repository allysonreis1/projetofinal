
package controller;

import java.util.ArrayList;
import model.Hotel;

public class ControleHotel {
    private ArrayList<Hotel> listaHoteis;
    
    public ControleHotel () {
        listaHoteis = new ArrayList<Hotel>();
    }

    public ArrayList<Hotel> getListaHoteis() {
        return listaHoteis;
    }

    public void setListaHoteis(ArrayList<Hotel> listaHoteis) {
        this.listaHoteis = listaHoteis;
    }
    
    public void adicionarHotel (Hotel umHotel) {
        this.listaHoteis.add(umHotel);
    }
    
    public void removerHotel (Hotel umHotel) {
        this.listaHoteis.remove(umHotel);
    }
    
    public Hotel pesquisarHotel (String umNome) {
        for (Hotel umHotel : listaHoteis) {
            if (umHotel.getNome().equalsIgnoreCase(umNome)) {
                return umHotel;
            }
        }
        return null;
    }
}
