
package controller;

import java.util.ArrayList;
import model.Cliente;

public class ControleCliente {
    private ArrayList<Cliente> listaClientes;
    
    public ControleCliente () {
        this.listaClientes = new ArrayList<Cliente>();
    }

    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }
    
    public void adicionarCliente (Cliente umCliente) {
        this.listaClientes.add(umCliente);
    }
    
    public Cliente pesquisarCliente (String umNome) {
        for (Cliente umCliente : listaClientes) {
            if (umCliente.getNome().equalsIgnoreCase(umNome)) {
                return umCliente; 
            }
        }
        return null;
    }
    
    public Cliente pesquisarCliente (String email, String senha) {
        for (Cliente umCliente : listaClientes) {
            if (umCliente.getEmail().equals(email) && umCliente.getSenha().equals(senha)) {
                return umCliente; 
            }
        }
        return null;
    }
    
    public void removerCliente (Cliente umCliente) {
        this.listaClientes.remove(umCliente);
    }
}
